#!/bin/bash

# mysql commands https://www.shellhacks.com/mysql-run-query-bash-script-linux-command-line/
# nextcloud installation https://techguides.yt/guides/how-to-install-and-configure-nextcloud-hub-21/

sudo timedatectl set-timezone Asia/Kolkata
sudo apt update
sudo apt install wget xclip pv cron unzip apache2 mysql-server php php-gd php-mysql php-curl php-mbstring php-intl php-gmp php-bcmath php-xml libapache2-mod-php php-zip php-apcu imagemagick-6-common php-imagick -y
sudo systemctl enable --now mysql
sudo systemctl enable --now apache2
read -p "Enter database name: " db
read -p "Enter Username for database: " user
stty -echo
read -p "Enter password for database: " pass
echo ""
read -p "Enter mysql root password: " rootpass
stty echo
sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$rootpass';
flush privileges;"
sudo mysql_secure_installation
sudo mysql -p$rootpass -u root -Bse "CREATE DATABASE $db;
CREATE USER '${user}'@'localhost' IDENTIFIED BY '${pass}';
GRANT ALL PRIVILEGES ON $db.* TO '${user}'@'localhost';
flush privileges;
quit"
wget -c https://download.nextcloud.com/server/releases/latest.zip
echo 'Unziping Nextcloud'
sudo unzip -o latest.zip -d /var/www | pv -l > /dev/null
cd /var/www
sudo chown -R www-data:www-data nextcloud/
sudo a2enmod headers env dir mime rewrite
read -p "Enter domain name: " dns
sudo touch /etc/apache2/sites-available/$dns.conf
echo '<VirtualHost *:80>

    ServerName dns
    ServerAlias www.dns
    DocumentRoot /var/www/nextcloud

    <Directory /var/www/nextcloud/>
        Require all granted
        AllowOverride All
        Options FollowSymLinks

        <IfModule mod_dav.c>
            Dav off
        </IfModule>

        SetEnv HOME /var/www/nextcloud
        SetEnv HTTP_HOME /var/www/nextcloud

        RewriteEngine On
        RewriteRule ^/\.well-known/carddav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
        RewriteRule ^/\.well-known/caldav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
        RewriteRule ^/\.well-known/host-meta https://%{SERVER_NAME}/public.php?service=host-meta [QSA,L]
        RewriteRule ^/\.well-known/host-meta\.json https://%{SERVER_NAME}/public.php?service=host-meta-json [QSA,L]
        RewriteRule ^/\.well-known/webfinger https://%{SERVER_NAME}/public.php?service=webfinger [QSA,L]

        <IfModule mod_headers.c>
           Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
        </IfModule>

    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>' | sudo tee -a /etc/apache2/sites-available/$dns.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns.conf
sudo a2dissite 000-default.conf
sudo a2dissite default-ssl.conf
sudo a2ensite $dns.conf
sudo service apache2 restart
read -p "open http://`hostname -I | cut -d' ' -f1` and press Enter" enter
if [ "$dns" != "" ]; then
sudo sed -i "9i\    \1 => '$dns'," /var/www/nextcloud/config/config.php
fi
sudo sed -i "11i\  \'default_phone_region' => 'IN'," /var/www/nextcloud/config/config.php
sudo sed -i "12i\  \'memcache.local' => '\\\OC\\\Memcache\\\APCu'," /var/www/nextcloud/config/config.php
sudo sed -i "13i\  \'htaccess.RewriteBase' => '/'," /var/www/nextcloud/config/config.php
php=$(php -v | cut -d' ' -f2 | head -n 1 | cut -d '.' -f1,2)
sudo sed -i 's/memory_limit = 128/memory_limit = 512/g' /etc/php/$php/apache2/php.ini
echo 'apc.enable_cli=1' | sudo tee -a /etc/php/$php/cli/php.ini > /dev/null
sudo -u www-data php /var/www/nextcloud/occ maintenance:update:htaccess
read -p "Paste this next window => */5  *  *  *  * php -f /var/www/nextcloud/cron.php" cron
sudo crontab -u www-data -e
sudo apt remove imagemagick-6-common php-imagick -y
sudo apt autoremove -y
sudo apt install imagemagick php-imagick -y
sudo service apache2 restart
echo "You have successfully installed NextCloud
Please visit http://`hostname -I | cut -d' ' -f1`"
read -p "Press ENTER to exit" bye
kill -9 $PPID
