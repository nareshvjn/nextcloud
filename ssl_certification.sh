#ssl certification https://techguides.yt/guides/free-wildcard-ssl-certificate-for-nextcloud-and-wordpress/

sudo snap install core; sudo snap refresh core
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot --apache
read -p "Enter domain name: " dns
sudo touch /etc/apache2/sites-available/$dns-ssl.conf
echo '<IfModule mod_ssl.c>
        <VirtualHost *:443>
               ServerName dns
               ServerAlias www.dns
               SSLEngine On
               SSLCertificateFile /etc/letsencrypt/live/dns/fullchain.pem
               SSLCertificateKeyFile /etc/letsencrypt/live/dns/privkey.pem
               Include /etc/letsencrypt/options-ssl-apache.conf
               DocumentRoot /var/www/nextcloud/

               <Directory /var/www/nextcloud/>
                   Require all granted
                   AllowOverride All
                   Options FollowSymLinks MultiViews

                   <IfModule mod_dav.c>
                    Dav off
                   </IfModule>

               SetEnv HOME /var/www/nextcloud
                   SetEnv HTTP_HOME /var/www/nextcloud

                   RewriteEngine On
                   RewriteRule ^/\.well-known/carddav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
                   RewriteRule ^/\.well-known/caldav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
                   RewriteRule ^/\.well-known/host-meta https://%{SERVER_NAME}/public.php?service=host-meta [QSA,L]
                   RewriteRule ^/\.well-known/host-meta\.json https://%{SERVER_NAME}/public.php?service=host-meta-json [QSA,L]
                   RewriteRule ^/\.well-known/webfinger https://%{SERVER_NAME}/public.php?service=webfinger [QSA,L]

                   <IfModule mod_headers.c>
                      Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
                   </IfModule>

               </Directory>

               ErrorLog ${APACHE_LOG_DIR}/error.log
               CustomLog ${APACHE_LOG_DIR}/access.log combined

       </VirtualHost>
</IfModule>' | sudo tee -a /etc/apache2/sites-available/$dns-ssl.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns-ssl.conf
sudo a2ensite $dns-ssl.conf
sudo a2enmod ssl
sudo a2enmod rewrite
sudo service apache2 restart
