# Adding Virtual Host
read -p "Enter domain name: " dns
sudo touch /etc/apache2/sites-available/$dns.conf
echo '<VirtualHost *:80>

    ServerName dns
    ServerAlias www.dns
    DocumentRoot /var/www/nextcloud

    <Directory /var/www/nextcloud/>
        Require all granted
        AllowOverride All
        Options FollowSymLinks

        <IfModule mod_dav.c>
            Dav off
        </IfModule>

        SetEnv HOME /var/www/nextcloud
        SetEnv HTTP_HOME /var/www/nextcloud

        RewriteEngine On
        RewriteRule ^/\.well-known/carddav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
        RewriteRule ^/\.well-known/caldav https://%{SERVER_NAME}/remote.php/dav/ [R=301,L]
        RewriteRule ^/\.well-known/host-meta https://%{SERVER_NAME}/public.php?service=host-meta [QSA,L]
        RewriteRule ^/\.well-known/host-meta\.json https://%{SERVER_NAME}/public.php?service=host-meta-json [QSA,L]
        RewriteRule ^/\.well-known/webfinger https://%{SERVER_NAME}/public.php?service=webfinger [QSA,L]
        
        <IfModule mod_headers.c>
           Header always set Strict-Transport-Security "max-age=15552000; includeSubDomains"
        </IfModule>

    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>' | sudo tee -a /etc/apache2/sites-available/$dns.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns.conf
sudo a2ensite $dns.conf
sudo service apache2 restart
